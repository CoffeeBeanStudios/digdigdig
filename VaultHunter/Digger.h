//
//  Digger.h
//  VaultHunter
//
//  Created by Rebekah Claypool on 11/9/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "CCNode.h"
#import "cocos2d.h"
#import "GameScene.h"
#import "MapLayer.h"

@class GameLayer;
@class MapLayer;

@interface Digger : CCNode{
    CCSprite *sprite;
    CCLayer *digLayer;
    GameLayer *game;
    CCAction *digAction;
    int direction;
    int currY, currX;
}

@property (nonatomic, retain) CCSprite *sprite;
@property (nonatomic, retain) GameLayer *game;
@property (nonatomic, retain) CCAction *digAction;
@property int direction;
@property int currX;
@property int currY;

-(id) initWithMap: (MapLayer *) map;
-(void) updateDigDirection: (int) digDirection;

@end
