//
//  Digger.m
//  VaultHunter
//
//  Created by Rebekah Claypool on 11/9/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "Digger.h"
#import "MapLayer.h"

@implementation Digger

@synthesize game, sprite, digAction, direction, currX, currY;

#define TEX_GROUND 1

-(id) initWithMap: (MapLayer *) map{
    self = [super init];
    
    if (self){
        //self.game = theGame;
        
        //digLayer = [[CCLayer alloc] init];
        //[digLayer addChild:self];
        //[map addChild:digLayer];
        
        
        [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:
         @"diggertexture.plist"];
        
        CCSpriteBatchNode *spriteSheet = [CCSpriteBatchNode
                                          batchNodeWithFile:@"diggertexture.png"];
        [self addChild:spriteSheet];
        //[self setScale:0.50];
        
        NSMutableArray *walkAnimFrames = [NSMutableArray array];
        //for(int i = 1; i <= 8; ++i) {
            [walkAnimFrames addObject:
             [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
              [NSString stringWithFormat:@"g8349.png", 0]]];
        
        [walkAnimFrames addObject:
         [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
          [NSString stringWithFormat:@"g8505.png", 1]]];
        
        //}
        
        CCAnimation *walkAnim = [CCAnimation 
                                 animationWithFrames:walkAnimFrames delay:0.5f];
        
        CGSize winSize = [CCDirector sharedDirector].winSize;
        sprite = [CCSprite spriteWithSpriteFrameName:@"g8505.png"];        
        sprite.position = ccp(0,0);//ccp(winSize.width/2, winSize.height/2);
        self.digAction = [CCRepeatForever actionWithAction:
                           [CCAnimate actionWithAnimation:walkAnim restoreOriginalFrame:NO]];
        [sprite  runAction:self.digAction];
        [spriteSheet addChild: sprite];
        
        currX = 10;
        currY = 5;
        
        [self updateDigDirection:2];
        //self.position = ccp(winSize.width/2, winSize.height/2);
        
                
        //add the sprite
        //sprite = [CCSprite spriteWithFile:@"digger.png"];
        //CCSpriteBatchNode *sSheet = [CCSpriteBatchNode batchNodeWithFile:@"groundtexture.png"];
        //[self addChild:sSheet z:1 tag: TEX_GROUND];
        
        //[sprite setTexture:[[CCTextureCache sharedTextureCache] 
        //sprite = [CCSprite spriteWithBatchNode:
        
        //[self addChild:sprite z:1];
        //[sprite setPosition:ccp(180,60)];
        //[sprite setScale:0.10];
    }
    
    return self;
}

-(CGRect) rect{
    //return [sprite.texture contentSize];
    CGRect c = CGRectMake(sprite.position.x-(sprite.textureRect.size.width/2) * sprite.scaleX, sprite.position.y-(sprite.textureRect.size.height/2)*sprite.scaleY, sprite.textureRect.size.width*sprite.scaleX, sprite.textureRect.size.height*sprite.scaleY);
    return c;
}

-(void) updateDigDirection: (int) digDirection{
    //NSLog(@"Digger Direction: %d", digDirection);
    self.direction = digDirection;
}


@end
