//
//  Map.m
//  VaultHunter
//
//  Created by Rebekah Claypool on 11/13/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "MapLayer.h"
#import "cocos2d.h"
#import "GameScene.h"
#import "Gold.h"
#import "CCLabelAtlas.h"

@implementation MapLayer

@synthesize tileMap = _tileMap;
@synthesize background = _background;
@synthesize spritelayer = _spritelayer;
@synthesize gold;


// Replace the init method with the following
-(id) init
{
    NSLog(@"in map");
    self=[super init];
    
    if(self) {
        currX = 9;
        currY = 4;
        
        NSLog(@"Cfreate texture");
        self.tileMap = [CCTMXTiledMap tiledMapWithTMXFile:@"map.tmx"];
        self.background = [self.tileMap layerNamed:@"foreground"];
        self.spritelayer = [self.tileMap layerNamed:@"spriteslayer"];
        
        if (!self.spritelayer){
            NSLog(@"Sprite Layer Empty");
        }
        
        gold = [[NSMutableArray alloc] init];

        
        //[self.tileMap setScale:0.50];
        [self.tileMap setPosition:ccp(0,0)];
        [self addChild:self.tileMap z:0];
        
        //sets the spawn and goal points
        [self setKeyPoints];
        
        [self setPosition: ccp(-1*spawnPoint.x, -1*spawnPoint.y)];
        
        currtileStrength = 3;//[self getTileStrengthAt: CGPointMake(currX, currY)];
        
        NSLog(@"Position %f, %f", spawnPoint.x, spawnPoint.y);
        
    }
    return self;
}

-(void) breakTileatX: (int) x andY:(int) y {
    currtileStrength = currtileStrength - 1; //1 will be the tool strength
    
    //see if we've killed the tile yet
    if (currtileStrength <= 0){
        currtileStrength = [self getTileStrengthAt: CGPointMake(currX, currY)];
        
        CCSprite *tile = [self.background tileAt:ccp(x, y)];
        [tile removeFromParentAndCleanup:YES];
        
        tile = [self.spritelayer tileAt:ccp(x, y)];
        [tile removeFromParentAndCleanup:YES];
        
        [self setPosition:CGPointMake(self.position.x, self.position.y + self.background.tileset.tileSize.height )];
        
        [self checkGoal];
    }
}

-(void) moveMap: (int) direction{
    NSLog(@"Direction: %d", direction);
    //move it up
    if (direction == 0){
        [self setPosition:CGPointMake(self.position.x, self.position.y - self.background.tileset.tileSize.height )];
    }
    else if (direction == 1){
        [self setPosition:CGPointMake(self.position.x + self.background.tileset.tileSize.height, self.position.y )];
        currX--;
    }
    else if (direction == 2){
        [self setPosition:CGPointMake(self.position.x, self.position.y + self.background.tileset.tileSize.height )];
        currY++;
    }
    else if (direction == 3){
        [self setPosition:CGPointMake(self.position.x - self.background.tileset.tileSize.height, self.position.y)];
        currX++;
    }
    
    CCSprite *tile = [self.background tileAt:ccp(currX, currY)];
    //currY++;
    currtileStrength = [self getTileStrengthAt: CGPointMake(currX, currY)];
    [tile removeFromParentAndCleanup:YES];
    
    tile = [self.spritelayer tileAt:ccp(currX, currY)];
    [tile removeFromParentAndCleanup:YES];
}

-(void) setKeyPoints{
    // Inside the init method, after setting self.background
    
    CCTMXObjectGroup *objectLayer = [_tileMap objectGroupNamed:@"objectlayer"];
    
    NSAssert(objectLayer != nil, @"'Objects' object group not found");
    
    NSMutableArray *objects = [objectLayer objects];
    
    for (int i = 0; i < [objects count]; i++){
        NSMutableArray *object = [objects objectAtIndex:i];
        
        NSString *type = [object valueForKey:@"type"]; 
        
        if ([type isEqualToString: @"spawn"]){
            spawnPoint.x = [[object valueForKey:@"x"] intValue] + 32; //should be half sprite size
            spawnPoint.y = [[object valueForKey:@"y"] intValue] + 15;  
        }
        else if ([type isEqualToString: @"goal"]){
            goalPoint.x = [[object valueForKey:@"x"] intValue] + 32; //should be half sprite size
            goalPoint.y = [[object valueForKey:@"y"] intValue] - 5;  
            NSLog(@"Raw goal Y: %f", goalPoint.y);
        }
        else if ([type isEqualToString: @"gold"]){
            //goalPoint.x = [[object valueForKey:@"x"] intValue] + 32; //should be half sprite size
            //goalPoint.y = [[object valueForKey:@"y"] intValue] - 5;  
            Gold *goldpiece = [[Gold alloc] init];
            
            [goldpiece setX: -1*[[object valueForKey:@"x"] floatValue]];
            [goldpiece setY: -1*[[object valueForKey:@"y"] floatValue]];
            [goldpiece setValue: [[object valueForKey:@"value"] intValue]]; //[object valueForKey:@"x"];
            
            [gold addObject:goldpiece];
            NSLog(@"Gold added");
        }
    }
}

-(void) checkGoal{
    /*
    NSLog(@"Goal: %f, Curr: %d", goalPoint.y, currY);
    if (goalPoint.y <= currY){
        NSLog(@"Goal!!!");
        
        // create and initialize a Label
		CCLabelTTF *label = [CCLabelTTF labelWithString:@"Goal!" fontName:@"Marker Felt" fontSize:64];
        
        // ask director the the window size
		CGSize size = [[CCDirector sharedDirector] winSize];
        
        
        
		// position on the center of the screen
		label.position =  ccp( size.width /2 , size.height/2 );
        [self addChild:label z:100];

    }*/
}

-(CGPoint) getSpawnPoint{
    // Inside the init method, after setting self.background
    CCTMXObjectGroup *objects = [_tileMap objectGroupNamed:@"objectlayer"];
    NSAssert(objects != nil, @"'Objects' object group not found");
    NSMutableDictionary *spawnPoint = [objects objectNamed:@"spawn"];        
    NSAssert(spawnPoint != nil, @"SpawnPoint object not found");
    int x = [[spawnPoint valueForKey:@"x"] intValue];
    int y = [[spawnPoint valueForKey:@"y"] intValue];
    
    NSLog(@"Position %d, %d", x, y);//[spawnPoint valueForKey:@"y"]);
    
    return CGPointMake(x, y);
}

-(float) getTileStrengthAt: (CGPoint) p{
    return 3;
}

-(CGPoint) getCurrPosition{
    return CGPointMake(0,0);//(currX, currY);
}

-(void) dealloc{
// In dealloc
self.tileMap = nil;
self.background = nil;
    [super dealloc];
}

@end
