//
//  HelloWorldLayer.m
//  VaultHunter
//
//  Created by Rebekah Claypool on 11/4/11.
//  Copyright __MyCompanyName__ 2011. All rights reserved.
//


// Import the interfaces
#import "GameScene.h"
#import "Digger.h"
#import "MapLayer.h"
#import "Gold.h"
#import "CCMask.h"

@implementation GameScene

@synthesize mapLayer, gameLayer, digMan;

-(id) init{
    self = [super init];
    
    NSLog(@"Init");
    
    if (self){
        //create the map layer
        //mapLayer = [[MapLayer alloc] initWithGame: self];
                
        //mapLayer = [[MapLayer alloc] init];
        mapLayer = [MapLayer node];
        //gameLayer = [[GameLayer alloc] init];
        gameLayer = [GameLayer node];
        
        digMan = [[Digger alloc] initWithMap: mapLayer];
        
        //[mapLayer setScale:0.95];
        
        [gameLayer addChild: digMan z:1];
        [gameLayer addChild: mapLayer z: 0];
        [self addChild: gameLayer z: 1];
    
        
        //[self addChild:digMan];
        
        NSLog(@"Before setMapLayer");
        [gameLayer setTheMapLayer: mapLayer];
        [gameLayer setDigMan:digMan];
        //[gameLayer setScale:0.50];
        //[mapLayer setPosition: ccp(-512,-12544)];
        //CCMoveTo *scroll = [CCMoveTo actionWithDuration: 10 position: ccp(512,-12000)];
        //[mapLayer runAction:scroll];
        //[mapLayer setScale:0.50];
        //[gameLayer setViewpointCenterOnMap: mapLayer];
        
    }
    
    return self;
}

- (void) dealloc{
    [super dealloc];
}

@end

//-------------Game Layer-----------

@implementation GameLayer

@synthesize mapLayer, digMan,startTouch, digSide;

#define DIG_LEFT 1
#define DIG_RIGHT 2

// on "init" you need to initialize your instance
-(id) init
{
	// always call "super" init
	// Apple recommends to re-assign "self" with the "super" return value
	if( (self=[super init])) {
		
		// create and initialize a Label
		//CCLabelTTF *label = [CCLabelTTF labelWithString:@"Test" fontName:@"Marker Felt" fontSize:64];

		// ask director the the window size
		CGSize size = [[CCDirector sharedDirector] winSize];
        
        
	
		// position on the center of the screen
		self.position =  ccp( size.width /2 , size.height/2 );
        
        self.isAccelerometerEnabled = YES;
        [[UIAccelerometer sharedAccelerometer] setUpdateInterval:(1.0 / 30)];
	}
	return self;
}

-(void) setTheMapLayer:(MapLayer*) map{
    NSLog(@"Setting map layer");
    self.mapLayer = map;
    
}

-(void)setViewpointCenterOnMap:(MapLayer *) map{
    NSLog(@"Set in center of screen");
    CGPoint position = digMan.position;
    
    NSLog(@"digger x=%f, y=%f", position.x, position.y);
    CGSize winSize = [[CCDirector sharedDirector] winSize];
    
    int x = MAX(position.x, winSize.width / 2);
    int y = MAX(position.y, winSize.height / 2);
    x = MIN(x, (map.tileMap.mapSize.width * map.tileMap.tileSize.width) 
            - winSize.width / 2);
    y = MIN(y, (map.tileMap.mapSize.height * map.tileMap.tileSize.height) 
            - winSize.height/2);
    CGPoint actualPosition = ccp(x, y);
    
    CGPoint centerOfView = ccp(winSize.width/2, winSize.height/2);
    CGPoint viewPoint = ccpSub(centerOfView, actualPosition);
    
    map.position = ccp(viewPoint.x-(winSize.width/2), viewPoint.y-(winSize.height/2));
    //digMan.position.x = centerOfView.x - digMan.sprite.s
    //digMan.position.x = centerOfView.x - digMan.sprite.size.width;
    
}

// on "dealloc" you need to release all your retained objects
- (void) dealloc
{
	// in case you have something to dealloc, do it in this method
	// in this particular example nothing needs to be released.
	// cocos2d will automatically release all the children (Label)
	
	// don't forget to call "super dealloc"
	[super dealloc];
}

-(void) onEnter{
    [[CCTouchDispatcher sharedDispatcher] addTargetedDelegate:self priority:0 swallowsTouches:YES];
    [super onEnter];
}

-(void) onExit{
    [[CCTouchDispatcher sharedDispatcher] removeDelegate:self];
    [super onExit];
}



-(BOOL) ccTouchBegan:(UITouch *)touch withEvent:(UIEvent *)event{
    startTouch = [touch locationInView:[touch view]];
    
    /*
    CCSprite *mask = [[CCSprite alloc] initWithFile:@"mask.png"];
	[mask setPosition: ccp(0,0)];
	CCMask *myMask = [CCMask createMaskForObject:mapLayer withMask:mask];
	[self addChild:myMask];
    [myMask mask];
    
    //in a update/event
    [[myMask maskSprite] setPosition: digMan.position];
    [myMask reDrawMask];
    */
     
    return YES;
}

-(void) ccTouchEnded:(UITouch *)touch withEvent:(UIEvent *)event{
    CGPoint location = [touch locationInView:[touch view]];
    CGSize winSize = [[CCDirector sharedDirector] winSize];
    
    //if it's on the left side move the game down
    if (location.x < (winSize.width/2)){
        [self digLeft];
    }
    else{
        [self digRight];
    }
}

-(void) digLeft{
    //see if I've already dug on this side
    if (digSide!=DIG_LEFT){
        //[mapLayer breakTileatX: digMan.currX andY: digMan.currY];
        [mapLayer moveMap:digMan.direction];
        //[self setViewpointCenterOnMap:mapLayer];
        [self checkObjectCollisions];
        
        digSide = DIG_LEFT;
    }
}
    
-(void) digRight{
    //see if I've already dug on this side
    if (digSide!=DIG_RIGHT){
        //[mapLayer breakTileatX: digMan.currX andY: digMan.currY];
        [mapLayer moveMap:digMan.direction];
        [self checkObjectCollisions];
        
        //[mapLayer setPosition:CGPointMake(mapLayer.position.x, mapLayer.position.y+5)];
        //[self setViewpointCenterOnMap:mapLayer];
        digSide = DIG_RIGHT;

    }
}

-(void) checkObjectCollisions{
    NSLog(@"CurrX: %f, %f", self.position.x, self.position.y);
    
    for (int i = 0; i < [mapLayer.gold count]; i++){
        Gold *g = [mapLayer.gold objectAtIndex:i];
        NSLog(@"Gold: %f, %f", g.x, g.y);
        
        if ((abs(mapLayer.position.x - g.x)<50) && (abs((mapLayer.position.y - g.y)) < 50)){
            CCLabelTTF *label1 = [CCLabelTTF labelWithString:@"Points" fontName:@"Arial" fontSize:20];
            
            label1.anchorPoint = ccp(0,0);
            
            CGSize size = [[CCDirector sharedDirector] winSize];
            label1.position = ccp(0,0);//ccp( size.width/2, size.height/2);
            
            [self addChild:label1 z:3 tag:0];
            
            CCFadeOut *action = [CCFadeOut actionWithDuration:1];
            [label1 runAction:action];
            
            
            NSLog(@"Collect Gold");
        }
    }
}

- (void)accelerometer:(UIAccelerometer *)accelerometer didAccelerate:(UIAcceleration *)acceleration {
    
    // Landscape left values
    float y = acceleration.y;
    
    //NSLog(@"Y: %f", y);
    
    //tilt left
    if (y< -0.2){
        //NSLog(@"Right");
        [digMan updateDigDirection: 3];
        //[mapLayer setPosition:CGPointMake(mapLayer.position.x+2, mapLayer.position.y)];
        
    }
    //tilt right
    else if (y>0.2){
        //NSLog(@"Left");
        [digMan updateDigDirection: 1];
        //[mapLayer setPosition:CGPointMake(mapLayer.position.x-2, mapLayer.position.y)];
    }
    else{
        //NSLog(@"Down");
        [digMan updateDigDirection: 2];
    }
}

@end
