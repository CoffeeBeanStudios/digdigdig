//
//  Gold.h
//  VaultHunter
//
//  Created by Rebekah Claypool on 12/4/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Gold : NSObject{
    int value;
    float x, y;
}

@property (nonatomic) int value;
@property (nonatomic) float x;
@property (nonatomic) float y;


@end
