//
//  HelloWorldLayer.h
//  VaultHunter
//
//  Created by Rebekah Claypool on 11/4/11.
//  Copyright __MyCompanyName__ 2011. All rights reserved.
//


// When you import this file, you import all the cocos2d classes
#import "cocos2d.h"
#import "Digger.h"
#import "MapLayer.h"
#import "GameScene.h"

@class Digger;
@class MapLayer;
@class GameLayer;

@interface GameScene : CCScene {
    MapLayer *mapLayer;
    GameLayer *gameLayer;
    Digger *digMan;
}

@property (nonatomic, retain) GameLayer *gameLayer;
@property (nonatomic, retain) MapLayer *mapLayer;
@property (nonatomic, retain) Digger *digMan;


// returns a CCScene that contains the HelloWorldLayer as the only child
//+(CCScene *) scene;

@end

// HelloWorldLayer
@interface GameLayer : CCLayer <CCTargetedTouchDelegate>{
    //UILabel *label;
    MapLayer *mapLayer;
    Digger *digMan;
    CGPoint startTouch;
    
    int digSide;
}

@property (nonatomic, retain) MapLayer *mapLayer;
@property (nonatomic, retain) Digger *digMan;
@property (nonatomic) CGPoint startTouch;
@property int digSide;

-(void) setTheMapLayer:(MapLayer*) map;
-(void)setViewpointCenterOnMap:(MapLayer *) map; 
-(void) digLeft;
-(void) digRight;
-(void) checkObjectCollisions;

@end
