//
//  Map.h
//  VaultHunter
//
//  Created by Rebekah Claypool on 11/13/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "CCNode.h"
#import "cocos2d.h"
#import "GameScene.h"

@interface MapLayer : CCLayer{
    CCTMXTiledMap *_tileMap;
    CCTMXLayer *_background;
    CCTMXLayer *_spritelayer;
    
    
    NSMutableArray *gold;
    
    float currtileStrength;
    
    CGPoint spawnPoint, goalPoint;
    
    int currX, currY;
}

// After the class declaration
@property (nonatomic, retain) CCTMXTiledMap *tileMap;
@property (nonatomic, retain) CCTMXLayer *background;
@property (nonatomic, retain) CCTMXLayer *spritelayer;
@property (nonatomic, retain) NSMutableArray *gold;

//create it and add it to the scene
//-(id) initWithGame: (CCScene *) gameScene;
-(CGPoint) getSpawnPoint;
-(void) breakTileatX: (int) x andY:(int) y;
-(float) getTileStrengthAt: (CGPoint) p;
-(void) moveMap: (int) direction;

-(void) setKeyPoints;
-(void) checkGoal;

@end
